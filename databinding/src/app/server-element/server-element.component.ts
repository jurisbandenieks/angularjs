import {
  Component,
  OnInit,
  Input,
  ViewEncapsulation,
  ViewChild,
  ElementRef
} from "@angular/core";

@Component({
  selector: "app-server-element",
  templateUrl: "./server-element.component.html",
  styleUrls: ["./server-element.component.css"],
  encapsulation: ViewEncapsulation.Emulated // None, Native
})
export class ServerElementComponent implements OnInit {
  @Input("serverElement") element: {
    type: string;
    name: string;
    content: string;
  };
  @ViewChild("heading", { static: true }) header: ElementRef;

  constructor() {}

  ngOnInit(): void {
    console.log(this.header.nativeElement.textContent);
  }
}
