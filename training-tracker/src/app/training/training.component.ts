import { Component, OnInit, OnDestroy } from "@angular/core";
import { Excercise } from "./excercise.model";
import { TrainingService } from "./training.service";
import { Subscription } from "rxjs";

@Component({
  selector: "app-training",
  templateUrl: "./training.component.html",
  styleUrls: ["./training.component.scss"],
})
export class TrainingComponent implements OnInit {
  onGoingTraining = false;
  excerciseSubscription: Subscription;

  constructor(private trainingExcercise: TrainingService) {}

  ngOnInit(): void {
    this.excerciseSubscription = this.trainingExcercise.excerciseChanged.subscribe(
      (excercise) => {
        if (excercise) {
          this.onGoingTraining = true;
        } else {
          this.onGoingTraining = false;
        }
      }
    );
  }

  ngOnDestroy() {
    this.excerciseSubscription.unsubscribe();
  }
}
