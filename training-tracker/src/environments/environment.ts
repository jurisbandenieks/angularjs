// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyC4HQ16ghROB-VlzUme9I8KbniyztNnKOU",
    authDomain: "training-tracker-715be.firebaseapp.com",
    databaseURL: "https://training-tracker-715be.firebaseio.com",
    projectId: "training-tracker-715be",
    storageBucket: "training-tracker-715be.appspot.com",
    messagingSenderId: "1098308223552",
    appId: "1:1098308223552:web:feb5d7ffbaea7204efd5a5",
  },
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
